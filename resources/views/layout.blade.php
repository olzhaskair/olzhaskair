<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Chokolife.me</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
@yield('content')
</body>
</html>