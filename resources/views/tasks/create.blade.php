@extends('layout')

@include('errors')


@section('content')
    <div class="container">
        <h3>Give feedback</h3>

        <div class="row">
            <div class="col-md-12">

                {!! Form::open(['route' => ['tasks.store']]) !!}
                <p>Your mark</p>
                <div class="form-group">
                    <input type="text" class="form control" name="title" value="{{old('title')}}">
                    <br>
                    <br>
                    <p>Write your review</p>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control">{{old('description')}}</textarea>
                    <br>
                    <button class="btn btn-danger">Complete</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection