@extends('layout')

@section('content')
<div class="container" style="display:inline-block;">
    <br>
    <br>
    <a href="{{ route('tasks.create') }}" class="btn btn-danger"> Give feedback <i class="glyphicon glyphicon-hand-left"></i></a>


    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <br>
            <hr> <h4>Feedbacks</h4> <hr>
            <table class="table">
                <thead>
                <tr>
                    <td>Marks</td>
                    <td>Reviews</td>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                <tr>
                    <td>{{$task->id}}</td>
                    <td>{{$task->title}}</td>
                    <td>
                        <a href="{{ route('tasks.show', $task->id) }}">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </a>
                        <a href="{{ route('tasks.edit', $task->id) }}">
                            <i class="glyphicon glyphicon-wrench"></i>
                        </a>
                        <a href="#">

                        </a>
                        {!! Form::open(['method' => 'DELETE',
                        'route' => ['tasks.destroy', $task->id]]) !!}
                        <button onclick="return confirm('Are you sure?')"> <i class="glyphicon glyphicon-minus"></i> </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection