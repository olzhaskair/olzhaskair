@extends('layout')

@include('errors')

@section('content')
<div class="container">
    <h3>Edit task # - {{ $task->id }}</h3>

    <div class="row">
        <div class="col-md-10">
            {!! Form::open(['route' => ['tasks.update', $task->id], 'method'=>'PUT']) !!}

            <div class="form-group">
                <input type="text" class="from-control" name="title" value="{{$task->title}}">
                <br>
                <textarea name="description" id="" cols="30" rows="10" class="from-control">{{$task->description}}</textarea>
                <br>
                <button class="btn btn-warning">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection

